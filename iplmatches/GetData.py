from .models import Deliveries, Matches
from collections import Counter
from django.db.models import Count, Sum, Avg, F, ExpressionWrapper, DecimalField
from django.db import transaction, IntegrityError

class QueryDataFromDb(object):
    """ 
    Makes queries from the database
    """

    def __init__(self):
        pass

    def matches_per_season(self):
        context = None
        try:
            with transaction.atomic():
                all_matches = Matches.objects.values("season").annotate(matches=Count("season"))
                get_matches_per_season = list(all_matches)
                print(get_matches_per_season)
                context = {"matches_per_season":get_matches_per_season}
        except IntegrityError:
                context = {"Error":"Something went wrong"}
        return context


    def total_wins_per_team(self):
        context = None
        try:
            with transaction.atomic():
                winners_in_season =  Matches.objects.values('season', 'winner').annotate(Count("winner")).order_by("season")
                seasons = set(obj["season"] for obj in Matches.objects.values('season'))
                context = {season:[] for season in seasons}
                for data in winners_in_season:
                    if data["season"] in context and data["winner"] != "":
                        context[data["season"]].append({"team":data["winner"],"total_wins":data["winner__count"]})
        except IntegrityError:
                context = {"Error":"Something went wrong"}
        
        return context


    def extraruns_by_team(self):
        context = None
        try:
            with transaction.atomic():
                extraruns_by_team = Matches.objects.filter(season=2016).annotate(bowling_team=F("deliveries__bowling_team")).values("bowling_team").annotate(extra_runs=Sum("deliveries__extra_runs"))
                context = {"2016_extra_runs":list(extraruns_by_team)}
        except IntegrityError:
                context = {"Error":"Something went wrong"}
        return context

    def economical_bowler(self):
        context = None
        try:
            with transaction.atomic():
                top_economical_bowler = Matches.objects.filter(season=2015).annotate(bowler=F("deliveries__bowler")
                ).values("bowler").annotate(economy=ExpressionWrapper(Sum("deliveries__total_runs")/(Count("deliveries__total_runs")/6), output_field=DecimalField())
                ).order_by("economy")[:10]
                top_economical_bowler = [{"bowler":query["bowler"], "economy":round(float(query["economy"]),2)} for query in top_economical_bowler]

                context = {"top_economical_bowler":list(top_economical_bowler)}
        except IntegrityError:
                context = {"Error":"Something went wrong"}
        return context

    def story(self):
        context = None
        try:
            with transaction.atomic():  
                yuvraj_runs =  Deliveries.objects.filter(batsman__icontains="Yuvraj").values("match_id").annotate(runs=Sum("batsman_run"))

                context = {"Yuvraj Sing":{ "runs_per_match" :list(yuvraj_runs)}}
        except IntegrityError:
                context = {"Error":"Something went wrong"}
        return context