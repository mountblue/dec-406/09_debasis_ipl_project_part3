import json
from django.conf import settings
from django.http import HttpResponse
from .GetData import QueryDataFromDb
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

queriedDatas = QueryDataFromDb()

# Create your views here.
@cache_page(CACHE_TTL)
def matches_per_season(request):
    context = queriedDatas.matches_per_season()
    return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(CACHE_TTL)
def total_wins_per_team(request):
    context = queriedDatas.total_wins_per_team()
    return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(CACHE_TTL)
def extraruns_by_team(request):
    context = queriedDatas.extraruns_by_team()
    return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(CACHE_TTL)
def economical_bowler(request):
     context = queriedDatas.economical_bowler()
     return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(CACHE_TTL)
def story(request):
     context = queriedDatas.story()
     return HttpResponse(json.dumps(context), content_type="application/json")
