import json
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from .GetData import QueryDataFromDb

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

queriedDatas = QueryDataFromDb()

def Home(request):
    return render(request, "iplmatches/home.html")

@cache_page(CACHE_TTL)
def matches_per_season(request):
    matches_data = queriedDatas.matches_per_season()
    data = {
        "matches_data":dict(matches_data),
        "title":"Matches Per Season",
        "text":"Number of Matches",
        "bottom":"Seasons"
    }
    json_data = json.dumps(data)
    return render(request, "iplmatches/matches.html", context={"data":json_data})

@cache_page(CACHE_TTL)
def wins_per_team(request):
    matches_data = queriedDatas.total_wins_per_team()
    data = {
        "matches_data":dict(matches_data),
        "title":"Wins per team in season ",
        "text":"Number of wins",
        "bottom":"Seasons"
    }
    json_data = json.dumps(data)
    return render(request, "iplmatches/winners.html", context={"data":json_data})

@cache_page(CACHE_TTL)
def extra_runs(request):
    matches_data = queriedDatas.extraruns_by_team()
    data = {
        "matches_data":dict(matches_data),
        "title":"Extra runs given by team in 2016",
        "text":"Extra runs",
        "bottom":"teams"
    }
    json_data = json.dumps(data)
    return render(request, "iplmatches/extra_runs.html", context={"data":json_data})

@cache_page(CACHE_TTL)
def economy(request):
    matches_data = queriedDatas.economical_bowler()
    data = {
        "matches_data":dict(matches_data),
        "title":"Top economical bowler of 2015",
        "text":"Economy",
        "bottom":"Bowlers"
    }
    json_data = json.dumps(data)
    return render(request, "iplmatches/economy.html", context={"data":json_data})

@cache_page(CACHE_TTL)
def story(request):
    matches_data = queriedDatas.story()
    data = {
        "matches_data":dict(matches_data),
        "title":"Yuvraj sing's runs per match",
        "text":"Runs",
        "bottom":"matches"
    }
    json_data = json.dumps(data)
    return render(request, "iplmatches/story.html", context={"data":json_data})


