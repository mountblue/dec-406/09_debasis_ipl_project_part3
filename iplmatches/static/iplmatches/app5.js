const chart5 = document.querySelector("#container5")
const data5 = JSON.parse(chart5.dataset.chart)
const datas = data5.matches_data["Yuvraj Sing"]["runs_per_match"].map(el => {
    return {name:el.match_id, y:el.runs}
})

Highcharts.chart('container5', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: data5.title
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true
        },
        showInLegend: true
      }
    },
    series: [{
      name: 'runs',
      colorByPoint: true,
      data:datas
    }]
  });