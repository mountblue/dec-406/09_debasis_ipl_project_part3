from django.urls import path
from . import  json_views, template_views

urlpatterns = [
    path("", template_views.Home, name="chart_total_matches"),
    path("matches/", template_views.matches_per_season, name="matches"),
    path("winners/", template_views.wins_per_team, name="winners"),
    path("extra_runs/", template_views.extra_runs, name="extra_runs"),
    path("economy/", template_views.economy, name="economy"),
    path("story/", template_views.story, name="view_story"),
    path("api/total_matches/", json_views.matches_per_season, name="total_matches"),
    path("api/wins_of_teams/", json_views.total_wins_per_team, name="wins_of_teams"),
    path("api/extra_runs/", json_views.extraruns_by_team, name="extra_runs"),
    path("api/top_bowlers/", json_views.economical_bowler, name="top_bowlers"),
    path("api/story/", json_views.story, name="story"),             
]